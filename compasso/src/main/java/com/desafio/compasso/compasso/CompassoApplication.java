package com.desafio.compasso.compasso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Marcelo Lopes
 *
 */
@SpringBootApplication
public class CompassoApplication {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(CompassoApplication.class, args);
	}

}
