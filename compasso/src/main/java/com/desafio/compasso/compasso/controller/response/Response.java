package com.desafio.compasso.compasso.controller.response;

import org.springframework.stereotype.Component;

import lombok.Data;

/**
 * @author Marcelo Lopes
 *
 * @param <T>
 */
@Component
@Data
public class Response<T> {

	private Integer status_code;
	
	private String message;
}