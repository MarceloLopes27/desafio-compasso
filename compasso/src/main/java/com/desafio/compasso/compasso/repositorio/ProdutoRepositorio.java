package com.desafio.compasso.compasso.repositorio;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.desafio.compasso.compasso.basicas.Produto;

/**
 * @author Marcelo Lopes
 *
 */
@Repository
public interface ProdutoRepositorio extends JpaRepository<Produto, String>{

	/**
	 * @param id
	 * @return
	 */
	@Query(value = "SELECT p FROM Produto p where p.id = :id")
	Produto consultarProdutoPorId(String id);

	/**
	 * @param q
	 * @param min_price
	 * @param max_price
	 * @return
	 */
	@Query(value = "SELECT p FROM Produto p where (:q is null OR p.description = :q) AND (:q is null OR p.name = :q)\r\n"
			+ "AND (:min_price is null OR p.price >= :min_price) \r\n"
			+ "AND (:max_price is null OR p.price <= :max_price)")
	List<Produto> consultarProdutoComParametros(@Param("q") String q , @Param("min_price") BigDecimal min_price, @Param("max_price") BigDecimal max_price);
}
