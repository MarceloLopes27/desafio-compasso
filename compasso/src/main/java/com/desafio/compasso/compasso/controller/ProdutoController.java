package com.desafio.compasso.compasso.controller;

import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.desafio.compasso.compasso.basicas.Produto;
import com.desafio.compasso.compasso.controller.response.Response;
import com.desafio.compasso.compasso.service.ProdutoServico;

/**
 * @author Marcelo Lopes
 *
 */
@RestController
@RequestMapping("/api/v1/product-ms")
@CrossOrigin(origins = "*")
public class ProdutoController {
	
	@Autowired
	private ProdutoServico servico;
	
	@Autowired
	private Response<Produto> response;
	
	/**
	 * @param request
	 * @param produto
	 * @param result
	 * @return
	 */
	@PostMapping(value = "/products", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> salvar(HttpServletRequest request, @RequestBody @Valid Produto produto, BindingResult result) {
		try {
			produto = this.servico.salvar(produto, result);
		} catch (BindException e) {
			return tratarBindException(e);
		}
		return new ResponseEntity<Produto>(produto, HttpStatus.CREATED);
	}

	
	/**
	 * @param id
	 * @param produto
	 * @param result
	 * @return
	 */
	@RequestMapping(value = "/products/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> atualizar(@PathVariable("id") String id, @RequestBody @Valid Produto produto, BindingResult result) {
		try {
			
			produto = this.servico.atualizar(id, produto, result);
			
		} catch (ResponseStatusException e) {
			
			response.setMessage(e.getReason());
			response.setStatus_code(e.getStatus().value());
			return new ResponseEntity<>(response, e.getStatus());
			
		} catch (BindException e) {
			return tratarBindException(e);
		}
		return new ResponseEntity<>(produto, HttpStatus.OK);
	}
	
	/**
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/products/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getPorId(@PathVariable("id") String id) {
		Produto produto;
		try {
			produto = this.servico.getPorId(id);
		} catch (ResponseStatusException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} 
		return new ResponseEntity<Produto>(produto, HttpStatus.OK);
	}
	
	/**
	 * @return
	 */
	@RequestMapping(value = "/products", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Produto>> getProdutos() {
		List<Produto> produtos = this.servico.getProdutos();
		return new ResponseEntity<List<Produto>>(produtos, HttpStatus.OK);
	}
	

	/**
	 * @param q
	 * @param min_price
	 * @param max_price
	 * @return
	 */
	@GetMapping(value = "/products/search", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> filtrarProdutos(@RequestParam(value = "q", required = false) String q,
			@RequestParam(value = "min_price", required = false) BigDecimal min_price,
			@RequestParam(value = "max_price", required = false) BigDecimal max_price) {
		List<Produto> produtos = this.servico.filtrarProdutos(q, min_price, max_price);
		return new ResponseEntity<List<Produto>>(produtos, HttpStatus.OK);
	}
	
	/**
	 * @param id
	 * @return
	 */
	@DeleteMapping(value = "/products/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> apagarProdutos(@PathVariable("id") String id) {
		HttpStatus status = this.servico.apagarProdutos(id);
		return new ResponseEntity<HttpStatus>(status);
	}
	
	/**
	 * @param e
	 * @return
	 */
	private ResponseEntity<Response<Produto>> tratarBindException(BindException e) {
		if (e.hasErrors()) {
			response.setMessage(e.getAllErrors().get(0).getDefaultMessage());
			response.setStatus_code(HttpStatus.BAD_REQUEST.value());
			return ResponseEntity.badRequest().body(response);
		}
		return null;
	}
	

}
