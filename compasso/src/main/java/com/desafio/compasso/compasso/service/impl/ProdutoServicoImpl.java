package com.desafio.compasso.compasso.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.server.ResponseStatusException;

import com.desafio.compasso.compasso.basicas.Produto;
import com.desafio.compasso.compasso.repositorio.ProdutoRepositorio;
import com.desafio.compasso.compasso.service.ProdutoServico;
import com.desafio.compasso.compasso.util.Constantes;

/**
 * @author Marcelo Lopes
 *
 */
@Service
public class ProdutoServicoImpl implements ProdutoServico {
	
	@Autowired
	private ProdutoRepositorio repositorio;

	/**
	 *
	 */
	@Override
	public @Valid Produto salvar(@Valid Produto produto, BindingResult result) throws BindException {
		if (result.hasErrors()) {
			throw new BindException(result);
		} 
		produto = repositorio.saveAndFlush(produto);
		return produto;
	}

	/**
	 *
	 */
	@Override
	public @Valid Produto atualizar(String id, @Valid Produto produto, BindingResult result) throws BindException, ResponseStatusException {
		
		if (result.hasErrors()) {
			throw new BindException(result);
		} 
		
		Produto produtoConsultado = repositorio.consultarProdutoPorId(id);
		
		if (Objects.isNull(produtoConsultado)) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, Constantes.PRODUTO_NAO_ENCONTRADO);
		} 
		
		produto = repositorio.saveAndFlush(produto);
		
		return produto;
	}

	/**
	 *
	 */
	@Override
	public Produto getPorId(String id) {
		
		Produto produtoConsultado = repositorio.consultarProdutoPorId(id);
		
		if (Objects.isNull(produtoConsultado)) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, Constantes.PRODUTO_NAO_ENCONTRADO);
		} 
		
		return produtoConsultado;
	}

	/**
	 *
	 */
	@Override
	public List<Produto> getProdutos() {
		
		List<Produto> list = repositorio.findAll();
		
		return list;
	}

	/**
	 *
	 */
	@Override
	public List<Produto> filtrarProdutos(String q, BigDecimal min_price, BigDecimal max_price) {

		List<Produto> produtos = repositorio.consultarProdutoComParametros(q , min_price, max_price );
		
		return produtos;
	}

	/**
	 *
	 */
	@Override
	public HttpStatus apagarProdutos(String id) {
		
		Produto produtoConsultado = repositorio.consultarProdutoPorId(id);
		
		if (Objects.isNull(produtoConsultado)) {
			return HttpStatus.NOT_FOUND;
		} 
		
		repositorio.delete(produtoConsultado);
		
		return HttpStatus.OK;
	}

}
