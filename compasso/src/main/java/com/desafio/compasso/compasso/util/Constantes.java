package com.desafio.compasso.compasso.util;

/**
 * @author Marcelo Lopes
 *
 */
public class Constantes {

	/**
	 * 
	 */
	public static final String PRODUTO_NAO_ENCONTRADO = "Produto não encontrado.";
	
	/**
	 * 
	 */
	public static final String CAMPO_NAME_NAO_PODE_SER_NULO = "Campo name não pode ser nulo.";
	
	/**
	 * 
	 */
	public static final String CAMPO_NOME_E_OBRIGATORIO = "Campo name é obrigatorio.";
	
	/**
	 * 
	 */
	public static final String CAMPO_DESCRIPTION_NAO_PODE_SER_NULO = "Campo description não pode ser nulo.";
	
	/**
	 * 
	 */
	public static final String CAMPO_DESCRIPTION_E_OBRIGATORIO = "Campo description é obrigatorio.";
	
	/**
	 * 
	 */
	public static final String CAMPO_PRICE_E_OBRIGATORIO = "Campo price é obrigatorio.";

	/**
	 * 
	 */
	public static final String CAMPO_PRICE_SO_PODE_SER_POSITIVO = "Campo price só pode ser positivo";
}
