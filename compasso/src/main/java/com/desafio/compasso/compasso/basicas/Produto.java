package com.desafio.compasso.compasso.basicas;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import org.hibernate.annotations.GenericGenerator;

import com.desafio.compasso.compasso.util.Constantes;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "produto")
public class Produto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid", strategy = "uuid")
	@Column(name = "id", nullable = false, unique = true)
	private String id; 
	
	@NotNull(message = Constantes.CAMPO_NAME_NAO_PODE_SER_NULO)
	@NotBlank(message = Constantes.CAMPO_NOME_E_OBRIGATORIO)
	@Column(name = "name", nullable = false)
	private String name;
	
	@NotNull(message = Constantes.CAMPO_DESCRIPTION_NAO_PODE_SER_NULO)
	@NotBlank(message = Constantes.CAMPO_DESCRIPTION_E_OBRIGATORIO)
	@Column(name = "description", nullable = false)
	private String description;
	
	@Positive(message = Constantes.CAMPO_PRICE_SO_PODE_SER_POSITIVO)
	@NotNull(message = Constantes.CAMPO_PRICE_E_OBRIGATORIO)
	@Column(name = "price", nullable = false, precision = 10, scale = 2)
	private BigDecimal price;
}
