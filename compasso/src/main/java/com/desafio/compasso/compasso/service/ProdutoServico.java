package com.desafio.compasso.compasso.service;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.server.ResponseStatusException;

import com.desafio.compasso.compasso.basicas.Produto;

/**
 * @author Marcelo Lopes
 *
 */
public interface ProdutoServico {

	/**
	 * @param produto
	 * @param result
	 * @return
	 * @throws BindException
	 */
	@Valid Produto salvar(@Valid Produto produto, BindingResult result) throws BindException;

	/**
	 * @param id
	 * @param produto
	 * @param result
	 * @return
	 * @throws BindException
	 * @throws ResponseStatusException
	 */
	@Valid Produto atualizar(String id, @Valid Produto produto, BindingResult result) throws BindException, ResponseStatusException;

	/**
	 * @param id
	 * @return
	 */
	Produto getPorId(String id);

	/**
	 * @return
	 */
	List<Produto> getProdutos();

	/**
	 * @param q
	 * @param min_price
	 * @param max_price
	 * @return
	 */
	List<Produto> filtrarProdutos(String q, BigDecimal min_price, BigDecimal max_price);

	/**
	 * @param id
	 * @return
	 */
	HttpStatus apagarProdutos(String id);

}
